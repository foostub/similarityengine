﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SEServices.Authentication;
using SEServer.Authentication;

namespace SE.Tests
{
    [TestClass]
    public class SEServerAuthorizationTests
    {
        [TestMethod]
        public void Valid_App_Name_Creates_App_Id_And_Api_Key()
        {
            var configuration = new Configuration("01234567", "", "", "");

            var service = new AuthorizationService(
                configuration: configuration,
                oauthServerOptions: new Microsoft.Owin.Security.OAuth.OAuthAuthorizationServerOptions()
            );

            var result = service.GenerateApiKey(applicationName: "test");

            Assert.IsNotNull(result);
            Assert.IsTrue(string.IsNullOrEmpty(result.ApiKey) == false);
            Assert.IsTrue(string.IsNullOrEmpty(result.AppId) == false);
        }
    }
}
