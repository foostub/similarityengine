﻿using Lucene.Net.Store;
using MerfunHeiger.SimilarityEngine;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.DataHandler.Serializer;
using Microsoft.Owin.Security.OAuth;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using SEServer.Authentication;
using SEServer.Controllers;
using SEServices.Authentication;
using SEServices.Items;
using System;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Results;

namespace SE.Tests
{
    [TestClass]
    public class SEServerControllerTests
    {
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            AutoMapper.Mapper.Initialize(cfg => {
                cfg.CreateMap<AddItemRequest, Item>();
                cfg.CreateMap<UpdateItemRequest, Item>();
                cfg.CreateMap<Item, ItemModel>();
            });
        }


        [TestMethod]
        public void Empty_App_Name_Returns_Bad_Request()
        {
            var configuration = new Configuration("012345678", "", "", "");

            var service = new AuthorizationService(
                configuration: configuration,
                oauthServerOptions: new OAuthAuthorizationServerOptions()
            );

            var controller = new AuthenticationController(
                authorizationService: service
            );

            var result = controller.GenerateApiKey("");

            Assert.IsTrue(result.GetType() == typeof(BadRequestResult));
        }

        [TestMethod]
        public void Valid_Api_Key_Generates_Service_Token()
        {
            var configuration = new Configuration("012345678", "", "", "");

            var provider = new SecureDataFormat<AuthenticationTicket>(DataSerializers.Ticket,
                new AesDataProtectorProvider(configuration.ServiceToken), TextEncodings.Base64);

            var oauthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AccessTokenFormat = provider,
                AuthorizationCodeFormat = provider,
                RefreshTokenFormat = provider
            };

            var service = new AuthorizationService(
                configuration: configuration,
                oauthServerOptions: oauthServerOptions
            );

            var controller = new AuthenticationController(
                authorizationService: service
            );

            var apiKey = service.GenerateApiKey("test");

            var result = controller.GetServiceToken(new ResourceTokenRequests { AppId = apiKey.AppId, ApiKey = apiKey.ApiKey });

            Assert.IsTrue(result.GetType().ToString().Contains("OkNegotiatedContentResult"));
        }

        [TestMethod]
        public void Added_Document_Is_Retrieved_Successfully()
        {
            using (var stream = new MemoryStream())
            {
                using (var db = new LiteDB.LiteDatabase(stream))
                {
                    var se = new SimilarityEngine(new RAMDirectory());
                    var repository = new LiteDbItemRepository(db);
                    var controller = new ItemsController(se, repository);

                    dynamic addResult = controller.Add(new AddItemRequest
                    {
                        Name = "test",
                        Brand = "brand",
                        Description = "desc",
                        Type = "type",
                        Categories = new string[0],
                        Price = 1
                    });

                    var id = addResult.Content.Id;

                    var item = controller.Get(id) as OkNegotiatedContentResult<ItemModel>;

                    Assert.IsTrue(item.Content.Id == id);
                }
            }
        }
    }
}
