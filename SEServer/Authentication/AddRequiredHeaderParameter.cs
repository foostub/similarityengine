﻿using System;
using System.Collections.Generic;
using System.Web.Http.Description;
using Swashbuckle.Swagger;

namespace SEServer.Authentication
{
	public class AddRequiredHeaderParameter : IOperationFilter
	{
		public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
		{
			if (operation.parameters == null)
			{
				operation.parameters = new List<Parameter>();
			}
			
			if (operation.operationId.StartsWith("Authentication") == false && operation.operationId.StartsWith("Background") == false) {
				operation.parameters.Add(new Parameter
				{
					name = "Authorization",
					@in = "header",
					type = "string",
					required = true
				});							
			}
		}
	}
}

