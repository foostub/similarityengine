﻿using log4net;
using Microsoft.Owin.Security.OAuth;
using SEServices.Authentication;
using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Web.Http;

namespace SEServer.Controllers
{
    public class AuthenticationController : ApiController
    {
        private static ILog logger = log4net.LogManager.GetLogger(typeof(AuthenticationController));

        private readonly IAuthorizationService _authorizationService;

        public AuthenticationController(
            IAuthorizationService authorizationService)
        {
            _authorizationService = authorizationService;
        }

        /// <summary>
        /// Generates an application id and api key from an application name for a service.
        /// </summary>
        [Route("api/1.0/authentication/service/api-key"), HttpGet]
        public IHttpActionResult GenerateApiKey(string appName)
        {
            if (string.IsNullOrEmpty(appName))
            {
                return this.BadRequest();
            }

            return this.Ok(_authorizationService.GenerateApiKey(appName));
        }

        /// <summary>
        /// Generates an authentiction token from an app id and api key.
        /// </summary>
        [Route("api/1.0/authentication/service/token"), HttpPost]
        public IHttpActionResult GetServiceToken(ResourceTokenRequests model)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(ModelState);
            }

            if (!_authorizationService.VerifyApiKey(model.AppId, model.ApiKey))
            {
                return this.Unauthorized();
            }

            var identity = new ClaimsIdentity(OAuthDefaults.AuthenticationType);
            identity.AddClaim(new Claim("ApplicationId", model.AppId));

            // TODO: where should the timeout live for the token?
            var token = _authorizationService.GenerateToken(identity, new TimeSpan(1, 0, 0));

            return this.Ok(new
            {
                Token = token
            });
        }

        /// <summary>
        /// Generates a refresh token from an expired service token.
        /// </summary>
        [Route("api/1.0/authentication/service/token/refresh"), HttpPost]
        public IHttpActionResult RefreshServiceToken()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Generates an impersonation token from a valid service token.
        /// </summary>
        [Route("api/1.0/authentication/service/token/impersonate"), HttpPost]
        public IHttpActionResult GenerateImpersonationToken()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Invalidate api key.
        /// </summary>
        [Route("api/1.0/authentication/service/api-key/invalidate"), HttpPost]
        public IHttpActionResult InvalidateApiKey(string applicationId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Invalidate server token.
        /// </summary>
        [Route("api/1.0/authentication/server/invalidate"), HttpPost]
        public IHttpActionResult InvalidateServerToken()
        {
            throw new NotImplementedException();
        }
    }

    public class ResourceTokenRequests
    {
        [Required]
        public string AppId { get; set; }
        [Required]
        public string ApiKey { get; set; }
    }
}
