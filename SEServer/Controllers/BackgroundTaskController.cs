﻿using SEServices.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace SEServer.Controllers
{
    public class BackgroundTaskController : ApiController
    {
        private readonly BackgroundTasks _tasks;

        public BackgroundTaskController(BackgroundTasks tasks)
        {
            _tasks = tasks;
        }

        /// <summary>
        /// Simple request/reply background job using MassTransit and RabbitMQ.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        [HttpGet, Route("api/1.0/backgroundtask/count")]
        public async Task<IHttpActionResult> Count(int number)
        {
            var count = await _tasks.Count(number);

            return this.Ok(new { number = count });
        }
    }
}
