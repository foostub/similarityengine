﻿using AutoMapper;
using log4net;
using MerfunHeiger.SimilarityEngine;
using MerfunHeiger.SimilarityEngine.Features;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SEServices.Items;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace SEServer.Controllers
{
    [Authorize]
    public class ItemsController : ApiController
    {
        private static ILog logger = log4net.LogManager.GetLogger(typeof(ItemsController));

        private readonly SimilarityEngine _similarityEngine;
        private readonly IItemRepository _items;

        public ItemsController(
            SimilarityEngine similarityEngine,
            IItemRepository items)
        {
            _similarityEngine = similarityEngine;
            _items = items;
        }

        [HttpGet, Route("api/1.0/items/commit")]
        public IHttpActionResult Commit()
        {
            _similarityEngine.Commit();

            return this.Ok();
        }

        [HttpGet, Route("api/1.0/items/optimize")]
        public IHttpActionResult Optimize()
        {
            // REFACTOR: this is a 'thing' left over from using
            // an old version of lucene. there should be a 
            // a way to refactor this.
            _similarityEngine.Optimize();

            return this.Ok();
        }

        [HttpGet, Route("api/1.0/items")]
        public IHttpActionResult Get(string itemId)
        {
            var item = _items.Get(itemId);

            if (item == null)
            {
                return this.NotFound();
            }

            var model = Mapper.Map<ItemModel>(item);

            return this.Ok(model);
        }


        [HttpPut, Route("api/1.0/items")]
        public IHttpActionResult Update(UpdateItemRequest model)
        {
            if (this.ModelState.IsValid == false)
            {
                return this.BadRequest(this.ModelState);
            }

            var item = Mapper.Map<Item>(model);
            _items.Add(item);

            var se = new SimilarObject(model.Id);
            _similarityEngine.AddSimilarObject(se);

            return this.Ok();
        }

        [HttpPost, Route("api/1.0/items")]
        public IHttpActionResult Add(AddItemRequest model)
        {
            if (this.ModelState.IsValid == false)
            {
                return this.BadRequest(this.ModelState);
            }

            var se = mapModel(model);
            _similarityEngine.AddSimilarObject(se);

            var item = Mapper.Map<Item>(model);
            item.Id = se.Id;
            _items.Add(item);

            return this.Ok(new { Id = item.Id });
        }

        [HttpDelete, Route("api/1.0/items")]
        public IHttpActionResult Delete(string id)
        {
            _similarityEngine.RemoveSimilarObject(id);
            _items.Delete(id);
            _similarityEngine.Commit();

            return this.Ok();
        }

        [HttpPost, Route("api/1.0/items/upload")]
        public async Task<IHttpActionResult> Upload()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                return this.BadRequest();
            }

            // HACK: what happens with the typical io errors?
            // ie - disk full, no permissions, etc
            if (System.IO.Directory.Exists("./uploads") == false)
            {
                System.IO.Directory.CreateDirectory("./uploads");
            }

            var provider = new MultipartFormDataStreamProvider("./uploads");

            var files = await Request.Content.ReadAsMultipartAsync(provider);

            if (files.FileData.Count == 1)
            {
                using (var reader = new JsonTextReader(new StreamReader(files.FileData.First().LocalFileName)))
                {
                    while (reader.Read())
                    {
                        if (reader.TokenType == JsonToken.StartObject)
                        {
                            var request = JObject.Load(reader).ToObject<UpdateItemRequest>();

                            var item = Mapper.Map<Item>(request);
                            _items.Add(item);

                            // REFACTOR: there should be better handling 
                            // for batch imports. a list of failed rows
                            // or options for 'fail all' or 'continue on fail'
                            var so = mapModel(request);

                            _similarityEngine.AddSimilarObject(so);
                        }
                    }
                }

                _similarityEngine.Commit();
                _similarityEngine.Optimize();

                File.Delete(files.FileData.First().LocalFileName);
            }

            return this.Ok();
        }

        [HttpPost, Route("api/1.0/items/query")]
        public IHttpActionResult Query(QueryItemsRequest query)
        {
            if (this.ModelState.IsValid == false)
            {
                return this.BadRequest(this.ModelState);
            }

            query.Start = query.Start.HasValue ? query.Start : 0;
            query.Count = query.Count.HasValue ? query.Count : 10;
            query.Max = query.Max.HasValue ? query.Max : 1000;

            var se = mapModel(query);

            var results = _similarityEngine.FindSimilarTo(se);

            var items = _items.Get(results.Select(I => I.Id).ToArray())
                .Select(I => Mapper.Map<ItemModel>(I));

            return this.Ok(items);
        }

        private SimilarObject mapModel(UpdateItemRequest model)
        {
            var so = new SimilarObject(model.Id);
            so.AddFeature(new TextFeature("name", model.Name));
            so.AddFeature(new TextFeature("description", model.Description));
            so.AddFeature(new TextFeature("brand", model.Brand));
            so.AddFeature(new NumericFeature("price", model.Price));
            so.AddFeature(new PropertySetFeature("category", model.Categories));
            so.AddFeature(new PropertySetFeature("type", new[] { model.Type }));
            return so;
        }

        private SimilarObject mapModel(AddItemRequest model)
        {
            var so = new SimilarObject(Guid.NewGuid().ToString());
            so.AddFeature(new TextFeature("name", model.Name));
            so.AddFeature(new TextFeature("description", model.Description));
            so.AddFeature(new TextFeature("brand", model.Brand));
            so.AddFeature(new NumericFeature("price", model.Price));
            so.AddFeature(new PropertySetFeature("category", model.Categories));
            so.AddFeature(new PropertySetFeature("type", new[] { model.Type }));
            return so;
        }

        // TODO: get rid of the duplicate model 
        private SimilarObjectQuery mapModel(QueryItemsRequest model)
        {
            var so = new SimilarObjectQuery();

            if (string.IsNullOrEmpty(model.Id) == false)
            {
                so.AddFeature(new TextFeature("id", model.Id));
            }

            if (string.IsNullOrEmpty(model.Name) == false)
            {
                so.AddFeature(new TextFeature("name", model.Name));
            }

            if (string.IsNullOrEmpty(model.Description) == false)
            {
                so.AddFeature(new TextFeature("description", model.Description));
            }

            if (string.IsNullOrEmpty(model.Brand) == false)
            {
                so.AddFeature(new TextFeature("brand", model.Brand));
            }

            if (model.Price.HasValue)
            {
                so.AddFeature(new NumericFeature("price", model.Price.Value));
            }

            if (model.Categories?.Length > 0)
            {
                so.AddFeature(new PropertySetFeature("category", model.Categories));
            }

            if (string.IsNullOrEmpty(model.Type) == false)
            {
                so.AddFeature(new PropertySetFeature("type", new[] { model.Type }));
            }

            return so;
        }
    }

    public class QueryResult
    {
        public QueryItemsRequest Query { get; set; }

        public int Total { get; set; }

        public ItemModel[] Results { get; set; }
    }

    public class AddItemRequest
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public string[] Categories { get; set; }

        public string Brand { get; set; }

        public string Type { get; set; }

        public float Price { get; set; }
    }

    public class UpdateItemRequest : AddItemRequest
    {
        [Required]
        public string Id { get; set; }
    }

    // REFACTOR: is there a way to combine the item
    // properties for the query without the required
    // data annotations?
    public class QueryItemsRequest 
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string[] Categories { get; set; }

        public string Brand { get; set; }

        public string Type { get; set; }

        public float? Price { get; set; }

        public int? Start { get; set; }
        public int? Count { get; set; }

        public int? Max { get; set; }
    }

    public class ItemModel : UpdateItemRequest
    {

    }
}
