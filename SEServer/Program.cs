﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using log4net.Config;
using MerfunHeiger.SimilarityEngine;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.Hosting;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.DataHandler.Serializer;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.StaticFiles;
using Newtonsoft.Json.Serialization;
using Owin;
using SEServer.Authentication;
using SEServer.Controllers;
using SEServer.Windsor;
using SEServices.Authentication;
using SEServices.Items;
using SEServices.Messages;
using Swashbuckle.Application;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace SEServer
{
    class Program
    {
        private static string _uiPath;

        private static void Startup(Owin.IAppBuilder app)
        {
            var config = new HttpConfiguration();

            config.MapHttpAttributeRoutes();

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            var container = new WindsorContainer();

            container.Register(Component
                .For<IConfiguration>()
                .UsingFactoryMethod(_ => new Configuration(
                        serviceToken: System.Configuration.ConfigurationManager.AppSettings["ServiceToken"].Replace("=", ""),
                        rabbitMqUri: System.Configuration.ConfigurationManager.AppSettings["RabbitMq.Url"],
                        rabbitMqUser: System.Configuration.ConfigurationManager.AppSettings["RabbitMq.User"],
                        rabbitMqPassword: System.Configuration.ConfigurationManager.AppSettings["RabbitMq.Password"]
                    ))
                .LifestyleSingleton()
            );

            container.Register(Classes.FromThisAssembly()
                .BasedOn<IHttpController>()
                .LifestyleTransient());

            config.DependencyResolver = new WindsorDependencyResolver(container);

            registerAuthorizationComponents(container, app);

            registerRepositories(container);

            registerLuceneComponents(container);

            container.Register(Component
                .For<BackgroundTasks>()
                .ImplementedBy<BackgroundTasks>()
                .LifestyleSingleton()
            );

            app.UseWebApi(config);

            _uiPath = _uiPath ?? @"./www";

            var physicalFileSystem = new PhysicalFileSystem(_uiPath);
            var options = new FileServerOptions
            {
                EnableDefaultFiles = true,
                FileSystem = physicalFileSystem,
                EnableDirectoryBrowsing = true
            };
            options.StaticFileOptions.FileSystem = physicalFileSystem;
            options.StaticFileOptions.ServeUnknownFileTypes = true;
            options.DefaultFilesOptions.DefaultFileNames = new[]
            {
                "index.html"
            };

            app.UseFileServer(options);

            configureSwagger(config);
        }

        private static void registerRepositories(IWindsorContainer container)
        {
            AutoMapper.Mapper.Initialize(cfg => {
                cfg.CreateMap<AddItemRequest, Item>();
                cfg.CreateMap<UpdateItemRequest, Item>();
                cfg.CreateMap<Item, ItemModel>();
            });

            container.Register(Component
                .For<IItemRepository>()
                .UsingFactoryMethod(_ => new LiteDbItemRepository(new LiteDB.LiteDatabase("./items.db")))
                .LifestyleSingleton()
            );
        }

        private static void registerLuceneComponents(IWindsorContainer container)
        {
            if (Directory.Exists("./index") == false)
            {
                Directory.CreateDirectory("./index");
            }

            var directory = Lucene.Net.Store.FSDirectory.Open("./index");
            var similarityEngine = new SimilarityEngine(directory);

            container.Register(Component
                .For<SimilarityEngine>()
                .UsingFactoryMethod(_ => similarityEngine)
                .LifestyleSingleton()
            );
        }

        private static void configureSwagger(HttpConfiguration config)
        {
            config
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("1.0", "");
                    c.UseFullTypeNameInSchemaIds();
                    var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    var commentsFileName = Assembly.GetExecutingAssembly().GetName().Name + ".XML";
                    var commentsFile = Path.Combine(baseDirectory, commentsFileName);
                    c.IncludeXmlComments(commentsFile);
                    c.OperationFilter<AddRequiredHeaderParameter>();
                })
                .EnableSwaggerUi();

        }

        private static void registerAuthorizationComponents(IWindsorContainer container, Owin.IAppBuilder app)
        {
            container.Register(Component
                .For<IAuthorizationService>()
                .ImplementedBy<AuthorizationService>()
                .LifestyleSingleton()
            );

            // the default oauth data protector provider can be used if running under windows.
            // mono didn't have an implementation for the default provider.
            var provider = new SecureDataFormat<AuthenticationTicket>(DataSerializers.Ticket,
                new AesDataProtectorProvider(container.Resolve<IConfiguration>().ServiceToken), TextEncodings.Base64);

            var oauthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AccessTokenFormat = provider,
                AuthorizationCodeFormat = provider,
                RefreshTokenFormat = provider
            };

            app.UseOAuthAuthorizationServer(oauthServerOptions);

            container.Register(Component.For<OAuthAuthorizationServerOptions>()
                .UsingFactoryMethod(() => oauthServerOptions)
                .LifestyleTransient());

            var bearerOptions = new OAuthBearerAuthenticationOptions
            {
                AccessTokenFormat = provider
            };

            container.Register(Component.For<OAuthBearerAuthenticationOptions>()
                .UsingFactoryMethod(() => bearerOptions)
                .LifestyleTransient());

            app.UseOAuthBearerAuthentication(bearerOptions);
        }


        static void Main(string[] args)
        {
            if (args.Length == 1)
            {
                _uiPath = args[0];
            }

            XmlConfigurator.Configure();

            checkServiceToken();

            using (WebApp.Start("http://*:8080/", app => Startup(app)))
            {
                Console.WriteLine("Server is listening on http://*:8080/");
                Console.WriteLine("Press any key to quit.");
                Console.ReadLine();
            }
        }

        private static void checkServiceToken()
        {
            // check to see if this instance has a service token.
            // the service token is used to by external services
            // to request an oauth token.
            var config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
            var serviceToken = config.AppSettings.Settings["ServiceToken"];
            if (serviceToken == null || string.IsNullOrEmpty(serviceToken.Value))
            {
                using (RandomNumberGenerator rng = new RNGCryptoServiceProvider())
                {
                    byte[] tokenData = new byte[32];
                    rng.GetBytes(tokenData);
                    var token = Convert.ToBase64String(tokenData);
                    config.AppSettings.Settings.Add("ServiceToken", token);
                }
                config.Save(System.Configuration.ConfigurationSaveMode.Minimal);
            }
        }
    }
}
