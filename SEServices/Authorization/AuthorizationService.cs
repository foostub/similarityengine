﻿using log4net;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using NeoSmart.Utils;
using SEServer.Authentication;
using System;
using System.Security.Claims;
using System.Security.Cryptography;

namespace SEServices.Authentication
{
    public class AuthorizationService : IAuthorizationService
    {
        private static ILog logger = log4net.LogManager.GetLogger(typeof(AuthorizationService));

        private readonly IConfiguration _configuration;
        private readonly OAuthAuthorizationServerOptions _oauthServerOptions;

        private const int SIZE = 32;
        private const int ITERATIONS = 300;

        public AuthorizationService(
            IConfiguration configuration,
            OAuthAuthorizationServerOptions oauthServerOptions)
        {
            _configuration = configuration;
            _oauthServerOptions = oauthServerOptions;
        }

        public AppApiKey GenerateApiKey(string applicationName)
        {
            using (var rng = new RNGCryptoServiceProvider())
            {
                var tokenData = new byte[32];
                rng.GetBytes(tokenData);

                var applicationId = UrlBase64.Encode(tokenData);
                var apiKey = generateApiKey(applicationId);

                return new AppApiKey
                {
                    AppId = applicationId,
                    ApiKey = apiKey
                };
            }
        }

        private string generateApiKey(string applicationId)
        {
            var salt = System.Text.ASCIIEncoding.UTF8.GetBytes(_configuration.ServiceToken);
            var password = System.Text.ASCIIEncoding.UTF8.GetBytes(applicationId);
            using (var generator = new Rfc2898DeriveBytes(password, salt, ITERATIONS))
            {
                return UrlBase64.Encode(generator.GetBytes(SIZE));
            }
        }

        public string GenerateToken(ClaimsIdentity identity, TimeSpan expiresIn)
        {
            var props = new AuthenticationProperties()
            {
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = DateTime.UtcNow.Add(expiresIn)
            };

            var ticket = new AuthenticationTicket(identity, props);

            return _oauthServerOptions.AccessTokenFormat.Protect(ticket);
        }

        public bool VerifyApiKey(string applicationId, string apiKey)
        {
            return generateApiKey(applicationId) == apiKey;
        }

        public ClaimsIdentity VerifyIdentity(string token)
        {
            throw new NotImplementedException();
        }
    }
}

