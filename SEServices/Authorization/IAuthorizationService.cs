﻿using System;
using System.Security.Claims;

namespace SEServices.Authentication
{
    public interface IAuthorizationService
	{
        AppApiKey GenerateApiKey(string applicationId);
        bool VerifyApiKey(string applicationId, string apiKey);
        string GenerateToken(ClaimsIdentity identity, TimeSpan expiresIn);
        ClaimsIdentity VerifyIdentity(string token);
	}

    public class AppApiKey
    {
        public string AppId { get; set; }
        public string ApiKey { get; set; }
    }
}

