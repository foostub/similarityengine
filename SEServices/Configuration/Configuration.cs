﻿using System;

namespace SEServer.Authentication
{
	public class Configuration : IConfiguration
    {
		public string ServiceToken { get; private set; }

        public string RabbitMqUri { get; private set; }

        public string RabbitMqUser { get; private set; }

        public string RabbitMqPassword { get; private set; }

        public Configuration (string serviceToken, string rabbitMqUri, string rabbitMqUser, string rabbitMqPassword)
		{
			this.ServiceToken = serviceToken;
            this.RabbitMqUri = rabbitMqUri;
            this.RabbitMqUser = rabbitMqUser;
            this.RabbitMqPassword = rabbitMqPassword;
		}
	}
}

