﻿namespace SEServer.Authentication
{
    public interface IConfiguration
    {
        string ServiceToken { get; }
        string RabbitMqUri { get; }
        string RabbitMqUser { get; }
        string RabbitMqPassword { get; }


    }
}