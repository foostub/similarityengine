﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEServices.Items
{
    public interface IItemRepository
    {
        void Save(Item item);
        void Add(Item item);
        void Delete(string id);
        Item Get(string id);
        Item[] Get(string[] ids);
    }

    public class Item
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string[] Categories { get; set; }

        public string Brand { get; set; }

        public string Type { get; set; }

        public float? Price { get; set; }
    }
}
