﻿using LiteDB;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEServices.Items
{
    public class LiteDbItemRepository : IItemRepository, IDisposable
    {
        private static ILog logger = log4net.LogManager.GetLogger(typeof(LiteDbItemRepository));

        private LiteDatabase _db;
        private LiteCollection<Item> _items;

        public LiteDbItemRepository(LiteDatabase db)
        {
            _db = db;
            _items = db.GetCollection<Item>();
            _items.EnsureIndex(I => I.Id);
        }

        public void Add(Item item)
        {
            logger.Debug($"adding item {item.Id}");
            _items.Upsert(item);
        }

        public void Delete(string id)
        {
            logger.Debug($"delete item {id}");
            _items.Delete(id);
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public Item[] Get(string[] ids)
        {
            // HACK: why isn't .Any query working here?
            //return _items.Find(Q => ids.Any(ID => ID == Q.Id)).ToArray();
            return ids.Select(I => _items.FindById(I)).ToArray();
        }

        public Item Get(string id)
        {
            return _items.FindById(id);
        }

        public void Save(Item item)
        {
            logger.Debug($"saving item {item.Id}");
            _items.Update(item);
        }
    }
}
