﻿using MassTransit;
using SEServer.Authentication;
using System;
using System.Threading.Tasks;

namespace SEServices.Messages
{
    public class BackgroundTasks : IDisposable
    {
        private readonly IBusControl _control;

        // TODO: verify this is threadsafe
        private readonly IRequestClient<IRequest, IReply> _client;

        public BackgroundTasks(IConfiguration configuration)
        {
            _control = Bus.Factory.CreateUsingRabbitMq(x =>
            {
                var host = x.Host(new Uri(configuration.RabbitMqUri), h =>
                {
                    h.Username(configuration.RabbitMqUser);
                    h.Password(configuration.RabbitMqPassword);
                });

                x.ReceiveEndpoint(host, "status_updates", e => e.Consumer<StatusUpdateConsumer>());
            });

            _control.StartAsync().Wait();

            _client = _control.CreateRequestClient<IRequest, IReply>(new Uri(configuration.RabbitMqUri + "request_service"), new TimeSpan(0, 0, 5));
        }

        public async Task<int> Count(int number)
        {
            var response = await _client.Request(new Request(number));
            return response.Number;
        }

        public void Dispose()
        {
            _control.Stop();
        }

        public class Request : IRequest
        {
            public Request(int number)
            {
                this.Number = number;
            }

            public int Number { get; private set; }
        }
    }
}
