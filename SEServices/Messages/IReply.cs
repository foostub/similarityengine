﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEServices.Messages
{
    public interface IReply
    {
        int Number { get; }
    }
}
