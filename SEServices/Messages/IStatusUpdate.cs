﻿namespace SEServices.Messages
{
    public interface IStatusUpdate
    {
        string Status { get; }
    }
}
