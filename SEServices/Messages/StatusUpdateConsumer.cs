﻿using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEServices.Messages
{
    public class StatusUpdateConsumer : IConsumer<IStatusUpdate>
    {
        public Task Consume(ConsumeContext<IStatusUpdate> context)
        {
            Console.WriteLine($"From background task: {context.Message.Status}");

            return Task.FromResult(0);
        }
    }
}
