﻿using MassTransit;
using SEServices.Messages;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEWorkerMT
{
    class Program
    {
        static void Main(string[] args)
        {
            var busControl = Bus.Factory.CreateUsingRabbitMq(x =>
            {
                var host = x.Host(new Uri(ConfigurationManager.AppSettings["RabbitMq.Url"]), h =>
                {
                    h.Username(ConfigurationManager.AppSettings["RabbitMq.User"]);
                    h.Password(ConfigurationManager.AppSettings["RabbitMq.Password"]);
                });

                x.ReceiveEndpoint(host, "request_service", e => e.Consumer<RequestConsumer>());
            });

            busControl.StartAsync().Wait();

            var timer = new System.Timers.Timer(1000);
            timer.Elapsed += (S, E) => busControl.Publish<IStatusUpdate>(new StatusUpdate(DateTime.Now.ToString()));
            timer.Start();

            Console.WriteLine("Press any key to stop the service...");
            Console.ReadKey();

            busControl.Stop();
        }
    }

    public class StatusUpdate : IStatusUpdate
    {

        public StatusUpdate(string status)
        {
            this.Status = status;
        }

        public string Status { get; private set; }

    }
}
