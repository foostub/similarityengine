﻿using MassTransit;
using SEServices.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEWorkerMT
{
    public class RequestConsumer : IConsumer<IRequest>
    {
        public async Task Consume(ConsumeContext<IRequest> context)
        {
            await context.RespondAsync(new Reply(context.Message.Number + 1));
        }

        public class Reply : IReply
        {
            public Reply(int number)
            {
                this.Number = number;
            }

            public int Number { get; private set; }
        }
    }
}
