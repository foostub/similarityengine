﻿using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search.Payloads;
using MerfunHeiger.SimilarityEngine.Features;
using MerfunHeiger.SimilarityEngine.LuceneNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MerfunHeiger.SimilarityEngine.Features
{
	public class DateTimeFeature : IFeature
	{
		public DateTimeFeature(string name, DateTime value)
		{
			this.Name = name;
			this.Value = value;
		}

		public DateTimeFeature(string name, DateTime value, float boost)
			: this(name, value)
		{
			this.QueryBoost = boost;
		}

		public DateTime Value { get; private set; }

		public string Name { get; private set; }

		public void Index(Lucene.Net.Documents.Document document)
		{
			var value = Convert.ToSingle(this.Value.ToOADate());
			var payloadTokenStream = new PayloadTokenStream(new string[] { "Value" }, new float[] { value });
			var field = new Field(this.Name, "", Field.Store.NO, Field.Index.NOT_ANALYZED, Field.TermVector.NO);
			field.SetTokenStream(payloadTokenStream);
			document.Add(field);			
		}

		public Lucene.Net.Search.Query PrepareQuery()
		{
			if (this.Value != DateTime.MinValue)
			{
				var q = new MerfunHeiger.SimilarityEngine.LuceneNET.PayloadTermQuery(new Term(this.Name, "Value"), new MaxPayloadFunction());
				q.Boost = this.QueryBoost == 0 ? 1 : this.QueryBoost;
				return q;
			}
			else
			{
				return null;
			}
		}

		public float QueryBoost { get; set; }

		public float Score(string term, byte[] buffer)
		{
			var storedValue = BitConverter.ToSingle(buffer, 0);
			var targetValue = Convert.ToSingle(this.Value.ToOADate());
			var r = Math.Min(targetValue, storedValue) / Math.Max(targetValue, storedValue);
			if (double.IsNaN(r) || double.IsPositiveInfinity(r) || double.IsNegativeInfinity(r))
			{
				return 0;
			}
			else
			{
				return r;
			}
		}
	}

}
