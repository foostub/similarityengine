﻿using Lucene.Net.Search;

namespace MerfunHeiger.SimilarityEngine.Features
{
    public interface IFeature
	{
		string Name { get; }
		void Index(Lucene.Net.Documents.Document document);
		Query PrepareQuery();
		float QueryBoost { get; set; }
		float Score(string term, byte[] payload);
	}

}
