﻿using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Search.Payloads;
using MerfunHeiger.SimilarityEngine.LuceneNET;
using sharonjl.utils;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MerfunHeiger.SimilarityEngine.Features
{
	public class LocationFeature : IFeature
	{
		public const int DEFAULT_QUERY_PRECISION = 3;
		public const int MAX_KILOMETERS = 10000;

		public float Latitude { get; private set; }
		public float Longitude { get; private set; }

		public string Name
		{
			get;
			private set;
		}

		public int QueryPrecision { get; set; }

		public float QueryBoost { get; set; }

		public LocationFeature(string name, float latitude, float longitude)
		{
			this.Name = name;
			this.Latitude = latitude;
			this.Longitude = longitude;
			this.QueryPrecision = DEFAULT_QUERY_PRECISION;
		}

		public LocationFeature(string name, float latitude, float longitude, float boost) : this(name, latitude, longitude)
		{
			this.QueryBoost = boost;
			this.QueryPrecision = DEFAULT_QUERY_PRECISION;
		}

		public LocationFeature(string name, float latitude, float longitude, float boost, int queryPrecision = DEFAULT_QUERY_PRECISION)
			: this(name, latitude, longitude)
		{
			this.QueryBoost = boost;
			this.QueryPrecision = queryPrecision;
		}

		public void Index(Lucene.Net.Documents.Document document)
		{
			// calculate and store hashes based on different levels of precision:
			// 3 - region
			// 4 - county
			// 5 - city
			// 6 - ~ 2.59 square km
			// 7 - ~ 3 blocks
			var geohashes = new List<string>();
			for (var i = 1; i < 8; i++)
			{
				geohashes.Add(Geohash.Encode(this.Latitude, this.Longitude, i));
			}

			var payloads = Enumerable.Repeat<byte[]>(ASCIIEncoding.ASCII.GetBytes(Geohash.Encode(this.Latitude, this.Longitude)), geohashes.Count);
			var payloadTokenStream = new PayloadTokenStream(geohashes.ToArray(), payloads.ToArray());
			var field = new Field(this.Name, "", Field.Store.NO, Field.Index.NOT_ANALYZED, Field.TermVector.NO);
			field.SetTokenStream(payloadTokenStream);
			document.Add(field);			
		}

		public Query PrepareQuery()
		{
			if (this.Latitude == 0 && this.Longitude == 0)
			{
				return null;
			}

			var hash = Geohash.Encode(this.Latitude, this.Longitude, this.QueryPrecision);

			// calculate bounding box
			var left = Geohash.CalculateAdjacent(hash, Geohash.Direction.Left);
			var top = Geohash.CalculateAdjacent(hash, Geohash.Direction.Top);
			var bottom = Geohash.CalculateAdjacent(hash, Geohash.Direction.Bottom);
			var right = Geohash.CalculateAdjacent(hash, Geohash.Direction.Right);
			var upperLeft = Geohash.CalculateAdjacent(left, Geohash.Direction.Top);
			var bottomLeft = Geohash.CalculateAdjacent(left, Geohash.Direction.Bottom);
			var upperRight = Geohash.CalculateAdjacent(right, Geohash.Direction.Top);
			var bottomRight = Geohash.CalculateAdjacent(right, Geohash.Direction.Bottom);

			var boundingBox = new Term[] { 
				new Term(this.Name, left), 
				new Term(this.Name, top),
				new Term(this.Name, right), 
				new Term(this.Name, bottom), 
				new Term(this.Name, upperLeft), 
				new Term(this.Name, upperRight), 
				new Term(this.Name, bottomLeft),
				new Term(this.Name, bottomRight)
			};

			// TODO: rewrite this as a GeohashQuery
			var bq = new BooleanQuery();
			foreach (var term in boundingBox)
			{
				var q = new MerfunHeiger.SimilarityEngine.LuceneNET.PayloadTermQuery(term, new MaxPayloadFunction());
				// HACK: revisit and find an easier way to pass a Term to the Similarity.ScorePayload function
				bq.Add(new BooleanClause(q, Occur.SHOULD));
			}
			bq.Boost = this.QueryBoost == 0 ? 1 : this.QueryBoost;
			return bq;
		}

		public float Score(string term, byte[] buffer)
		{
			var hash = ASCIIEncoding.ASCII.GetString(buffer);
			var decode = Geohash.Decode(hash);
			var queryCoords = new GeoCoordinate(this.Latitude, this.Longitude);
			var storedCoords = new GeoCoordinate(decode[0], decode[1]);
			var distanceInKm = (float)queryCoords.GetDistanceTo(storedCoords) / 1000;
			return MAX_KILOMETERS - distanceInKm; // <-- HACK: will need a better way of calculating sort order for the distance
		}
	}
}
