﻿using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Search.Function;
using Lucene.Net.Search.Payloads;
using MerfunHeiger.SimilarityEngine.LuceneNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MerfunHeiger.SimilarityEngine.Features
{
	public class NumericFeature : IFeature
	{
		public string Name { get; private set; }
		public float Value { get; private set; }
		public float QueryBoost { get; set; }

		public NumericFeature(string name, float value)
		{
			this.Name = name;
			if (value < 0)
			{
				throw new ArgumentOutOfRangeException("value cannot be negative");
			}
			this.Value = value;
		}

		public NumericFeature(string name, float value, float boost) : this(name, value)
		{
			this.QueryBoost = boost;
		}

		public void Index(Document document)
		{
			if (this.Value > 0)
			{
				// TODO: verify ValueSource, CustomScorer and Field_Cache aren't faster
				var payloadTokenStream = new PayloadTokenStream(new string[] { "Value" }, new float[] { this.Value });
				var field = new Field(this.Name, "", Field.Store.NO, Field.Index.NOT_ANALYZED, Field.TermVector.NO);
				field.SetTokenStream(payloadTokenStream);
				document.Add(field);			
			}
		}

		public Query PrepareQuery()
		{
			if (this.Value == 0)
			{
				return null;
			}
			// 
			var q = new MerfunHeiger.SimilarityEngine.LuceneNET.PayloadTermQuery(new Term(this.Name, "Value"), new MaxPayloadFunction());
			q.Boost = this.QueryBoost == 0 ? 1 : this.QueryBoost;
			return q;
		}

		public float Score(string term, byte[] buffer)
		{
			// HACK: might go back to ValueSource for this but will need to find a way to pass a reference 
			// so this Score function can be called
			var storedValue = BitConverter.ToSingle(buffer, 0);
			var r = Math.Min(this.Value, storedValue) / Math.Max(this.Value, storedValue);
			if (double.IsNaN(r) || double.IsPositiveInfinity(r) || double.IsNegativeInfinity(r))
			{
				return 0;
			}
			else
			{
				return r;
			}

		}
	}
}
