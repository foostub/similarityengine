﻿using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using MerfunHeiger.SimilarityEngine.LuceneNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MerfunHeiger.SimilarityEngine.Features
{
	public class PropertySetFeature : IFeature
	{
		public string Name { get; private set; }
		public List<string> Properties { get; private set; }
		public float QueryBoost { get; set; }

		public PropertySetFeature(string name)
		{
			this.Name = name;
			this.Properties = new List<string>();
		}

		public PropertySetFeature(string name, float boost) : this(name)
		{
			this.QueryBoost = boost;
		}

		public PropertySetFeature(string name, IEnumerable<string> values)
			: this(name)
		{
			this.Properties.AddRange(values);
		}

		public PropertySetFeature(string name, IEnumerable<string> values, float boost) : this(name, values)
		{
			this.QueryBoost = boost;
		}
		public void AddProperty(string name)
		{
			this.Properties.Add(name);
		}

		public void Index(Document document)
		{
			if (this.Properties.Count > 0)
			{
				var propertySetTokenStream = new PropertySetTokenStream(this.Properties.ToArray());
				var field = new Field(this.Name, "", Field.Store.NO, Field.Index.NOT_ANALYZED, Field.TermVector.NO);
				field.SetTokenStream(propertySetTokenStream);
				document.Add(field);
			}
		}

		public Query PrepareQuery()
		{
			if (this.Properties.Count == 0)
			{
				return null;
			}
			var bq = new BooleanQuery();
			foreach (var property in this.Properties)
			{
				bq.Add(new BooleanClause(new TermQuery(new Term(this.Name, property)), Occur.SHOULD));
			}
			bq.Boost = this.QueryBoost == 0 ? .25f : this.QueryBoost;
			return bq;
		}

		public float Score(string term, byte[] storedValue)
		{
			throw new NotImplementedException();
		}
	}

}
