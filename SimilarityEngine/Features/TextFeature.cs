﻿using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using System;
using System.IO;

namespace MerfunHeiger.SimilarityEngine.Features
{
    public class TextFeature : IFeature
    {
        public string Name { get; private set; }

        public string Text { get; private set; }

        public float QueryBoost { get; set; }

        private Analyzer _analyzer;

        private QueryParser _parser;

        public TextFeature(string name, string text)
            : this(name, text, new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30))
        {
        }

        public TextFeature(string name, string text, Analyzer analyzer)
        {
            this.Name = name;
            this.Text = text;
            this._analyzer = analyzer;
            this._parser = new QueryParser(Lucene.Net.Util.Version.LUCENE_30, this.Name, this._analyzer);
        }

        public void Index(Document document)
        {
            var field = new Field(this.Name, this.Text, Field.Store.NO, Field.Index.ANALYZED);
            field.SetTokenStream(this._analyzer.TokenStream(this.Name, new StringReader(this.Text)));
            document.Add(field);
        }

        public Query PrepareQuery()
        {
            var query = this._parser.Parse(this.Text);
            query.Boost = .10f;
            return query;
        }

        public float Score(string term, byte[] payload)
        {
            return 1;
        }
    }
}
