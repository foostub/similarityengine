﻿using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Search.Payloads;
using MerfunHeiger.SimilarityEngine.LuceneNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MerfunHeiger.SimilarityEngine.Features
{
	public class WeightedPropertySetFeature : IFeature
	{
		private Dictionary<string, float> _properties = new Dictionary<string, float>();

		public WeightedPropertySetFeature(string name)
		{
			this.Name = name;
		}

		public WeightedPropertySetFeature(string name, float boost) : this(name)
		{
			this.QueryBoost = boost;
		}

		public float QueryBoost { get; set; }

		public void AddProperty(string name, float weight)
		{
			_properties[name] = weight;
		}

		public float GetPropertyValue(string name)
		{
			if (_properties.ContainsKey(name))
			{
				return _properties[name];
			}
			else
			{
				return 0.0f;
			}
		}

		public string Name { get; private set; }

		public void Index(Document document)
		{
			if (this._properties.Count > 0)
			{
				var payloadTokenStream = new PayloadTokenStream(_properties.Keys.ToArray(), _properties.Values.ToArray());
				var field = new Field(this.Name, "", Field.Store.NO, Field.Index.NOT_ANALYZED, Field.TermVector.NO);
				field.SetTokenStream(payloadTokenStream);
				document.Add(field);
			}
		}

		public Query PrepareQuery()
		{
			if (this._properties.Count == 0)
			{
				return null;
			}
			var bq = new BooleanQuery();
			foreach (var property in _properties)
			{
				//var q = new PayloadTermQuery(new Term(this.Name, property.Key), new MaxPayloadFunction());
				// HACK: revisit and find an easier way to pass a Term to the Similarity.ScorePayload function
				var q = new MerfunHeiger.SimilarityEngine.LuceneNET.PayloadTermQuery(new Term(this.Name, property.Key), new MaxPayloadFunction());
				q.Boost = property.Value;
				bq.Add(new BooleanClause(q, Occur.SHOULD));
			}
			bq.Boost = this.QueryBoost == 0 ? 1 : this.QueryBoost;
			return bq;
		}

		public float Score(string term, byte[] buffer)
		{
			var storedValue = BitConverter.ToSingle(buffer, 0);
			if (_properties.ContainsKey(term))
			{
				var r = Math.Min(_properties[term], storedValue) / Math.Max(_properties[term], storedValue);
				if (double.IsNaN(r) || double.IsPositiveInfinity(r) || double.IsNegativeInfinity(r))
				{
					return 0;
				}
				else
				{
					return r;
				}
			}
			else
			{
				return 0;
			}
		}
	}
}
