﻿/**
 *  Copyright (C) 2011 by Sharon Lourduraj
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  ------------------------------------------------------------------------------
 *  
 *  This code is a direct derivation from:
 *      GeoHash Routines for Javascript 2008 (c) David Troy. 
 *  The source of which can be found at: 
 *      https://github.com/davetroy/geohash-js
 */
using System;
using System.Collections.Generic;

namespace sharonjl.utils
{
	// HACK: ugly bit of code to simplify the syntax for the neighbor/boundary sets
	public class NestedHashset
	{
		private Dictionary<string, NestedHashset> _values = new Dictionary<string, NestedHashset>();

		public NestedHashset()
		{

		}

		public NestedHashset this[string key]
		{
			get
			{
				if (_values.ContainsKey(key) == false)
				{
					_values.Add(key, new NestedHashset());
				}
				return _values[key];
			}
			set
			{
				_values[key] = value;
			}
		}

		public string Value { get; set; }

		public static implicit operator NestedHashset(string value)
		{
			var n = new NestedHashset();
			n.Value = value;
			return n;
		}
	}

	public static class Geohash
	{
		public enum Direction
		{
			Left,
			Right,
			Top,
			Bottom
		}

		private static int[] BITS = new int[] { 16, 8, 4, 2, 1 };
		private static string BASE32 = "0123456789bcdefghjkmnpqrstuvwxyz";
		private static NestedHashset NEIGHBORS = new NestedHashset();
		private static NestedHashset BORDERS = new NestedHashset();
		
		static Geohash()
		{
			NEIGHBORS["right"]["even"] = "bc01fg45238967deuvhjyznpkmstqrwx";
			NEIGHBORS["left"]["even"] = "238967debc01fg45kmstqrwxuvhjyznp";
			NEIGHBORS["top"]["even"] = "p0r21436x8zb9dcf5h7kjnmqesgutwvy";
			NEIGHBORS["bottom"]["even"] = "14365h7k9dcfesgujnmqp0r2twvyx8zb";

			BORDERS["right"]["even"] = "bcfguvyz";
			BORDERS["left"]["even"] = "0145hjnp";
			BORDERS["top"]["even"] = "prxz";
			BORDERS["bottom"]["even"] = "028b";

			NEIGHBORS["bottom"]["odd"] = NEIGHBORS["left"]["even"];
			NEIGHBORS["top"]["odd"] = NEIGHBORS["right"]["even"];
			NEIGHBORS["left"]["odd"] = NEIGHBORS["bottom"]["even"];
			NEIGHBORS["right"]["odd"] = NEIGHBORS["top"]["even"];

			BORDERS["bottom"]["odd"] = BORDERS["left"]["even"];
			BORDERS["top"]["odd"] = BORDERS["right"]["even"];
			BORDERS["left"]["odd"] = BORDERS["bottom"]["even"];
			BORDERS["right"]["odd"] = BORDERS["top"]["even"];
		}

		private static void refine_interval(ref double[] interval, int cd, int mask)
		{
			if ((cd & mask) != 0)
				interval[0] = (interval[0] + interval[1]) / 2;
			else
				interval[1] = (interval[0] + interval[1]) / 2;
		}

		public static double[] Decode(string geohash)
		{
			var is_even = true;
			var lat = new double[3];
			var lon = new double[3];
			var mask = 0;

			lat[0] = -90.0; lat[1] = 90.0;
			lon[0] = -180.0; lon[1] = 180.0;
			var lat_err = 90.0; var lon_err = 180.0;

			for (var i = 0; i < geohash.Length; i++)
			{
				var c = geohash[i];
				var cd = BASE32.IndexOf(c);
				for (var j = 0; j < 5; j++)
				{
					mask = BITS[j];
					if (is_even)
					{
						lon_err /= 2;
						refine_interval(ref lon, cd, mask);
					}
					else
					{
						lat_err /= 2;
						refine_interval(ref lat, cd, mask);
					}
					is_even = !is_even;
				}
			}
			lat[2] = (lat[0] + lat[1]) / 2;
			lon[2] = (lon[0] + lon[1]) / 2;

			return new double[] { lat[2], lon[2] };		
		}

		public static string Encode(double latitude, double longitude, int precision = 12)
		{
			var is_even = true;
			var lat = new double[2];
			var lon = new double[2];
			var bit = 0;
			var ch = 0;
			var geohash = "";
			var mid = 0.0;

			lat[0] = -90.0; lat[1] = 90.0;
			lon[0] = -180.0; lon[1] = 180.0;

			while (geohash.Length < precision)
			{
				if (is_even)
				{
					mid = (lon[0] + lon[1]) / 2;
					if (longitude > mid)
					{
						ch |= BITS[bit];
						lon[0] = mid;
					}
					else
						lon[1] = mid;
				}
				else
				{
					mid = (lat[0] + lat[1]) / 2;
					if (latitude > mid)
					{
						ch |= BITS[bit];
						lat[0] = mid;
					}
					else
						lat[1] = mid;
				}

				is_even = !is_even;
				if (bit < 4)
					bit++;
				else
				{
					geohash += BASE32[ch];
					bit = 0;
					ch = 0;
				}
			}
			return geohash;
		}

		public static string CalculateAdjacent(string hash, Direction direction)
		{
			var dir = direction.ToString().ToLower();
			var srcHash = hash.ToLower();
			var lastChr = srcHash[srcHash.Length - 1];
			var type = srcHash.Length % 2 == 0 ? "odd" : "even";
			var @base = srcHash.Substring(0, srcHash.Length - 1);
			if (BORDERS[dir][type].Value.IndexOf(lastChr) != -1)
				@base = CalculateAdjacent(@base, direction);
			return @base + BASE32[NEIGHBORS[dir][type].Value.IndexOf(lastChr)];
		}
	}
}