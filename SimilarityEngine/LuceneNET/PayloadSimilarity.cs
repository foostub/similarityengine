﻿using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Search.Payloads;
using MerfunHeiger.SimilarityEngine.Features;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MerfunHeiger.SimilarityEngine.LuceneNET
{
	public class PayloadSimilarity : DefaultSimilarity
	{
		private SimilarObjectQuery _similarObjectQuery;

		public PayloadSimilarity(SimilarObjectQuery similarObjectQuery)
		{
			_similarObjectQuery = similarObjectQuery;
		}

		public override float ScorePayload(Int32 docId, String fieldName, Int32 start, Int32 end, Byte[] payload, Int32 offset, Int32 length)
		{
			throw new NotImplementedException("Use ScorePayload((Int32 docId, String fieldName, Int32 start, Int32 end, Byte[] payload, Int32 offset, Int32 length, Term term) instead");
		}

		public float ScorePayload(Int32 docId, String fieldName, Int32 start, Int32 end, Byte[] payload, Int32 offset, Int32 length, Term term)
		{
			var buffer = new byte[length];
			Buffer.BlockCopy(payload, offset, buffer, 0, length);
			var targetFeature = _similarObjectQuery.GetFeature(fieldName);
			if (targetFeature != null)
			{
				return targetFeature.Score(term.Text, buffer);
			}
			else
			{
				return 0;
			}
		}
	}
}
