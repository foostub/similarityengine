﻿using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Search.Payloads;
using Lucene.Net.Search.Spans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MerfunHeiger.SimilarityEngine.LuceneNET
{
	/// <summary>
	/// Modified from original at http://lucenenet.apache.org/docs/3.0.3/d0/dd7/_payload_term_query_8cs_source.html because
	/// Similarity.ScorePayload needs to know about the current term to find the target value. The original implementation does not
	/// include a PayloadTermWeight and PayloadTermSpanScorer that can be inherited.
	/// </summary>
	[Serializable]
	public class PayloadTermQuery : SpanTermQuery
	{
		protected internal PayloadFunction function;
		private bool includeSpanScore;

		public PayloadTermQuery(Term term, PayloadFunction function)
			: this(term, function, true)
		{
			
		}

		public PayloadTermQuery(Term term, PayloadFunction function, bool includeSpanScore)
			: base(term)
		{
			this.function = function;
			this.includeSpanScore = includeSpanScore;
		}

		public override Weight CreateWeight(Searcher searcher)
		{
			return new PayloadTermWeight(this, this, searcher);
		}

		[Serializable]
		protected internal class PayloadTermWeight : SpanWeight
		{
			private void InitBlock(PayloadTermQuery enclosingInstance)
			{
				this.enclosingInstance = enclosingInstance;
			}
			private PayloadTermQuery enclosingInstance;
			public PayloadTermQuery Enclosing_Instance
			{
				get
				{
					return enclosingInstance;
				}

			}

			public PayloadTermWeight(PayloadTermQuery enclosingInstance, PayloadTermQuery query, Searcher searcher)
				: base(query, searcher)
			{
				InitBlock(enclosingInstance);
			}

			public override Scorer Scorer(IndexReader reader, bool scoreDocsInOrder, bool topScorer)
			{
				return new PayloadTermSpanScorer(this, (TermSpans)internalQuery.GetSpans(reader), this, similarity as PayloadSimilarity, reader.Norms(internalQuery.Field));
			}

			protected internal class PayloadTermSpanScorer : SpanScorer
			{
				private void InitBlock(PayloadTermWeight enclosingInstance)
				{
					this.enclosingInstance = enclosingInstance;
				}
				private PayloadTermWeight enclosingInstance;
				public PayloadTermWeight Enclosing_Instance
				{
					get
					{
						return enclosingInstance;
					}

				}
				// TODO: is this the best way to allocate this?
				protected internal byte[] payload = new byte[256];
				protected internal TermPositions positions;
				protected internal float payloadScore;
				protected internal int payloadsSeen;

				public PayloadTermSpanScorer(PayloadTermWeight enclosingInstance, TermSpans spans, Weight weight, PayloadSimilarity similarity, byte[] norms)
					: base(spans, weight, similarity, norms)
				{
					InitBlock(enclosingInstance);
					positions = spans.Positions;
				}

				public /*protected internal*/ override bool SetFreqCurrentDoc()
				{
					if (!more)
					{
						return false;
					}
					doc = spans.Doc();
					freq = 0.0f;
					payloadScore = 0;
					payloadsSeen = 0;
					PayloadSimilarity similarity1 = Similarity as PayloadSimilarity;
					while (more && doc == spans.Doc())
					{
						int matchLength = spans.End() - spans.Start();

						freq += similarity1.SloppyFreq(matchLength);
						ProcessPayload(similarity1);

						more = spans.Next(); // this moves positions to the next match in this
						// document
					}
					return more || (freq != 0);
				}

				protected internal virtual void ProcessPayload(PayloadSimilarity similarity)
				{
					if (positions.IsPayloadAvailable)
					{
						payload = positions.GetPayload(payload, 0);
						//
						payloadScore = Enclosing_Instance.Enclosing_Instance.function.CurrentScore(doc, Enclosing_Instance.Enclosing_Instance.internalTerm.Field, spans.Start(), spans.End(), payloadsSeen, payloadScore, similarity.ScorePayload(doc, Enclosing_Instance.Enclosing_Instance.internalTerm.Field, spans.Start(), spans.End(), payload, 0, positions.PayloadLength, this.enclosingInstance.enclosingInstance.Term));
						payloadsSeen++;
					}
					else
					{
						// zero out the payload?
					}
				}

				/// <summary> </summary>
				/// <returns> <see cref="GetSpanScore()" /> * <see cref="GetPayloadScore()" />
				/// </returns>
				/// <throws>  IOException </throws>
				public override float Score()
				{

					return Enclosing_Instance.Enclosing_Instance.includeSpanScore ? GetSpanScore() * GetPayloadScore() : GetPayloadScore();
				}

				/// <summary> Returns the SpanScorer score only.
				/// <p/>
				/// Should not be overriden without good cause!
				/// 
				/// </summary>
				/// <returns> the score for just the Span part w/o the payload
				/// </returns>
				/// <throws>  IOException </throws>
				/// <summary> 
				/// </summary>
				/// <seealso cref="Score()">
				/// </seealso>
				[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
				protected internal virtual float GetSpanScore()
				{
					return base.Score();
				}

				/// <summary> The score for the payload
				/// 
				/// </summary>
				/// <returns> The score, as calculated by
				/// <see cref="PayloadFunction.DocScore(int, String, int, float)" />
				/// </returns>
				[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
				protected internal virtual float GetPayloadScore()
				{
					return Enclosing_Instance.Enclosing_Instance.function.DocScore(doc, Enclosing_Instance.Enclosing_Instance.internalTerm.Field, payloadsSeen, payloadScore);
				}

				protected override Explanation Explain(int doc)
				{
					ComplexExplanation result = new ComplexExplanation();
					Explanation nonPayloadExpl = base.Explain(doc);
					result.AddDetail(nonPayloadExpl);
					// QUESTION: Is there a way to avoid this skipTo call? We need to know
					// whether to load the payload or not
					Explanation payloadBoost = new Explanation();
					result.AddDetail(payloadBoost);

					float payloadScore = GetPayloadScore();
					payloadBoost.Value = payloadScore;
					// GSI: I suppose we could toString the payload, but I don't think that
					// would be a good idea
					payloadBoost.Description = "scorePayload(...)";
					result.Value = nonPayloadExpl.Value * payloadScore;
					result.Description = "btq, product of:";
					result.Match = nonPayloadExpl.Value == 0 ? false : true; // LUCENE-1303
					return result;
				}
			}
		}

		public override int GetHashCode()
		{
			int prime = 31;
			int result = base.GetHashCode();
			result = prime * result + ((function == null) ? 0 : function.GetHashCode());
			result = prime * result + (includeSpanScore ? 1231 : 1237);
			return result;
		}

		public override bool Equals(System.Object obj)
		{
			if (this == obj)
				return true;
			if (!base.Equals(obj))
				return false;
			if (GetType() != obj.GetType())
				return false;
			PayloadTermQuery other = (PayloadTermQuery)obj;
			if (function == null)
			{
				if (other.function != null)
					return false;
			}
			else if (!function.Equals(other.function))
				return false;
			if (includeSpanScore != other.includeSpanScore)
				return false;
			return true;
		}
	}
}