﻿using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Tokenattributes;
using Lucene.Net.Index;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MerfunHeiger.SimilarityEngine.LuceneNET
{
	public class PayloadTokenStream : TokenStream
	{
		private string[] _tokens;
		private byte[][] _payloads;
		private ITermAttribute _termAttr;
		private IPayloadAttribute _payloadAttr;
		private int _currentPosition = -1;

		public PayloadTokenStream(IEnumerable<string> tokens, IEnumerable<byte[]> payloads)
		{
			_tokens = tokens.ToArray();
			_payloads = payloads.ToArray();
			_termAttr = AddAttribute<ITermAttribute>();
			_payloadAttr = AddAttribute<IPayloadAttribute>();
		}

		public PayloadTokenStream(string[] tokens, float[] payloads)
		{
			_tokens = tokens;
			_payloads = payloads.Select(P => BitConverter.GetBytes(P)).ToArray();
			_termAttr = AddAttribute<ITermAttribute>();
			_payloadAttr = AddAttribute<IPayloadAttribute>();
		}

		protected override void Dispose(bool disposing)
		{

		}

		public override bool IncrementToken()
		{
			ClearAttributes();
			_currentPosition++;

			if (_tokens.Length == 0 || _currentPosition == _tokens.Length)
			{
				return false;
			}

			_termAttr.SetTermBuffer(_tokens[_currentPosition]);
			_termAttr.SetTermLength(_tokens[_currentPosition].Length);
			_payloadAttr.Payload = new Payload(_payloads[_currentPosition]);

			return _currentPosition != _tokens.Length;
		}

		public override void Reset()
		{
			base.Reset();
			_currentPosition = -1;
		}
	}
}
