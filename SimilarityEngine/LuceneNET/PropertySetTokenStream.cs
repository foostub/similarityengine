﻿using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Tokenattributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MerfunHeiger.SimilarityEngine.LuceneNET
{
	public class PropertySetTokenStream : TokenStream
	{
		private string[] _tokens;
		private ITermAttribute _termAttr;
		private int _currentPosition = -1;

		public PropertySetTokenStream(string[] tokens)
		{
			_tokens = tokens;
			_termAttr = AddAttribute<ITermAttribute>();
		}

		protected override void Dispose(bool disposing)
		{

		}

		public override bool IncrementToken()
		{
			ClearAttributes();
			_currentPosition++;

			if (_tokens.Length == 0 || _currentPosition == _tokens.Length)
			{
				return false;
			}

			_termAttr.SetTermBuffer(_tokens[_currentPosition]);
			_termAttr.SetTermLength(_tokens[_currentPosition].Length);

			return _currentPosition != _tokens.Length;
		}

		public override void Reset()
		{
			base.Reset();
			_currentPosition = -1;
		}
	}
}
