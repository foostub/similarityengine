﻿using System.ComponentModel.DataAnnotations;

namespace MerfunHeiger.SimilarityEngine
{
    public class SimilarObject : SimilarObjectBase
	{
		public string Id { get; private set; }

		public SimilarObject(string id)
		{
			this.Id = id;
		}
	}

}
