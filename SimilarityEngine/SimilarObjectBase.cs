﻿using MerfunHeiger.SimilarityEngine.Features;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MerfunHeiger.SimilarityEngine
{
	public abstract class SimilarObjectBase
	{
		private Dictionary<string, IFeature> _features = new Dictionary<string, IFeature>();

		public IEnumerable<IFeature> Features { get { return _features.Values; } }

		public IFeature GetFeature(string featureName)
		{
			return _features[featureName];
		}

		public void AddFeature(IFeature feature)
		{
			if (feature.Name.StartsWith("__"))
			{
				throw new ArgumentOutOfRangeException("feature names that start with __ are reserved");
			}
			_features[feature.Name] = feature;
		}
	}

}
