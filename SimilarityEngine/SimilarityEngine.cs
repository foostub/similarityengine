﻿using log4net;
using Lucene.Net.Analysis;
using Lucene.Net.Contrib.Management;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Store;
using MerfunHeiger.SimilarityEngine.LuceneNET;
using System;
using System.Collections.Generic;

namespace MerfunHeiger.SimilarityEngine
{
    public class SimilarityEngine : IDisposable
	{
        private static ILog logger = log4net.LogManager.GetLogger(typeof(SimilarityEngine));

        private readonly Directory _directory;
		private readonly IndexWriter _writer;
        private readonly SearcherManager _manager;

        public SimilarityEngine(Directory directory)
		{
            _directory = directory;
			if (!Lucene.Net.Index.IndexReader.IndexExists(_directory))
			{
				_writer = new Lucene.Net.Index.IndexWriter(_directory, analyzer(), true, Lucene.Net.Index.IndexWriter.MaxFieldLength.UNLIMITED);
			}
			else
			{
				try
				{
					_writer = new Lucene.Net.Index.IndexWriter(_directory, analyzer(), false, Lucene.Net.Index.IndexWriter.MaxFieldLength.UNLIMITED);
				}
				catch (Lucene.Net.Store.LockObtainFailedException ex)
				{
					_directory.DeleteFile("write.lock");
					_writer = new Lucene.Net.Index.IndexWriter(_directory, analyzer(), false, Lucene.Net.Index.IndexWriter.MaxFieldLength.UNLIMITED);
				}
			}
			_writer.UseCompoundFile = false;

            _manager = new SearcherManager(_writer);
		}

		private Analyzer analyzer()
		{
			return new KeywordAnalyzer();
		}

		public void AddSimilarObject(SimilarObject similarObject)
		{
			if (similarObject == null)
			{
				throw new ArgumentNullException("similarObject");
			}
			if (string.IsNullOrEmpty(similarObject.Id))
			{
				throw new ArgumentOutOfRangeException("id cannot be empty or null");
			}
			var document = new Document();
			document.Add(new Field("__id", similarObject.Id, Field.Store.YES, Field.Index.NOT_ANALYZED));
			foreach (var feature in similarObject.Features)
			{
				feature.Index(document);
			}
			_writer.UpdateDocument(new Term("__id", similarObject.Id), document);
		}

		public IEnumerable<ScoredObject> FindSimilarTo(SimilarObjectQuery similarQuery, int max = 1000, int start = 0, int count = 10)
		{
			var query = new BooleanQuery();
			foreach (var feature in similarQuery.Features)
			{
				var q = feature.PrepareQuery();
				if (q != null)
				{
					query.Add(new BooleanClause(q, Occur.SHOULD));
				}
			}

            using (var token = _manager.Acquire())
            {
                token.Searcher.Similarity = new PayloadSimilarity(similarQuery);
                var docs = token.Searcher.Search(query, max);
                for (var i = start; i < count && i < max && i < docs.TotalHits; i++)
                {
                    var document = token.Searcher.Doc(docs.ScoreDocs[i].Doc);
                    yield return new ScoredObject(document.Get("__id"), docs.ScoreDocs[i].Score / docs.MaxScore);
                }
            }
		}

		public void Optimize()
		{
			this._writer.Optimize();
		}

		public void Commit()
		{
			_writer.Commit();
		}

		public void RemoveSimilarObject(string id)
		{
			_writer.DeleteDocuments(new Term("__id", id));
		}

		public void Dispose()
		{
			_directory.Dispose();
		}

		public class ScoredObject
		{
			public double Score { get; private set; }
			public string Id { get; private set; }

			public ScoredObject(string id, double score)
			{
				this.Id = id;
				this.Score = score;
			}

			public override string ToString()
			{
				return this.Score.ToString("N4") + " " + this.Id;
			}
		}
	}

}
